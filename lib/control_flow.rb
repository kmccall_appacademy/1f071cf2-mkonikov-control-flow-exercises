# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char.reject { |ch| ch == ch.downcase }.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  middle = str.length / 2
  str.length.even? ? str[middle-1..middle] : str[middle]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.count('aeiou')
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  arr.reduce("") do | a, el |
    if a == ""
      a << el
    else
      a << separator + el
    end
  end
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird = str
  weird.chars.map.with_index { |ch, i|  i.even? ? weird[i] = ch.downcase : weird[i] = ch.upcase }.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split.each { |w| w.reverse! if w.length > 4 }.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result = []
  (1..n).each do | n |
    if n % 3 == 0 && n % 5 == 0
      result << "fizzbuzz"
    elsif n % 3 == 0
      result << "fizz"
    elsif n % 5 == 0
      result << "buzz"
    else
      result << n
    end
  end
  result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed = []
  arr.each do |el|
    reversed.unshift(el)
  end
  reversed
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2
  (2...num).each do |n|
    return false if num % n == 0
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).select do |n|
    num % n == 0
  end
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |n| prime?(n) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd = []
  even = []
  arr.each { |n| n.even? ? even << n : odd << n }
  odd.length == 1 ? odd[0] : even[0]
end
